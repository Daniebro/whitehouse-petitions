//
//  Petition.swift
//  Project7
//
//  Created by Danni Brito on 11/9/19.
//  Copyright © 2019 Danni Brito. All rights reserved.
//

import Foundation

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}
